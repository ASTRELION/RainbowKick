import discord
from discord.ext import commands
import typing

class General(commands.Cog, name = "Rainbow Commands"):
    """General commands"""

    def __init__(self, client):

        super().__init__()
        self.client = client


    @commands.command("ping")
    async def ping(self, ctx):

        latency = self.client.latency
        await ctx.send("Pong! *{}ms*".format(round(latency * 1000)))


    @commands.command("kick")
    @commands.guild_only()
    @commands.bot_has_permissions(kick_members = True)
    @commands.has_permissions(kick_members = True)
    async def kick(self, ctx, user: discord.User, *, reason: typing.Optional[str] = None):
        """Arbitrarily kick a user from the server"""
        
        guild = ctx.guild
        await guild.kick(user, reason = reason)


    @commands.group("set")
    @commands.guild_only()
    @commands.has_permissions(administrator = True)
    async def setCmd(self, ctx):

        if (not ctx.invoked_subcommand):
            await ctx.send("`set` requires a subcommand.")


    @setCmd.command("kickmsg")
    async def set_kickmsg(self, ctx, *, message: str):
        """Set the message to display when a user is kicked"""

        self.client.write_config(data = {
            "kick_msg": message
        })

    
    @setCmd.command("system")
    async def set_system(self, ctx, channel: typing.Optional[discord.TextChannel] = None):
        """Set the system messages channel"""

        if (not channel):
            channel = ctx.guild.system_channel

        self.client.write_config(data = {
            "system_channel": channel.id
        })


    @setCmd.command("welcome")
    async def set_welcome(self, ctx, channel: typing.Optional[discord.TextChannel] = None):
        """Set the welcome messages channel"""

        if (not channel):
            channel = ctx.guild.system_channel

        self.client.write_config(data = {
            "welcome_channel": channel.id
        })


    @setCmd.command("timeout")
    async def set_timeout(self, ctx, timeout: int):
        """Set the timeout before kicking an inactive user"""

        self.client.write_config(data = {
            "timeout": timeout
        })


    @setCmd.command("role")
    async def set_role(self, ctx, role: discord.Role):
        """Set the role for active users"""

        self.client.write_config(data = {
            "role": role.id
        })


    @commands.group("reset")
    @commands.guild_only()
    @commands.has_permissions(administrator = True)
    async def reset(self, ctx):

        if (not ctx.invoked_subcommand):
            await ctx.send("`reset` requires a subcommand.")


    @reset.command("all")
    async def reset_all(self, ctx):
        """Reset all configuration to default values"""

        for reset_cmd in self.client.get_command("reset").commands:
            if (reset_cmd.name != "all"):
                await ctx.invoke(self.client.get_command(reset_cmd.qualified_name))


    @reset.command("kickmsg")
    async def reset_kickmsg(self, ctx):
        """Reset the kick message to default"""

        self.client.write_config(data = {
            "kick_msg": "%USER% was kicked from the server."
        })


    @reset.command("system")
    async def reset_system(self, ctx):
        """Reset the system channel to default"""

        self.client.write_config(data = {
            "system_channel": ctx.guild.system_channel.id
        })


    @reset.command("welcome")
    async def reset_welcome(self, ctx):
        """Reset the welcome channel to default"""

        self.client.write_config(data = {
            "welcome_channel": ctx.guild.system_channel.id
        })


    @reset.command("timeout")
    async def reset_timeout(self, ctx):
        """Reset the timeout time to default"""

        self.client.write_config(data = {
            "timeout": 30
        })


    @reset.command("role")
    async def reset_role(self, ctx):
        """Reset the role for active users to default @everyone role"""

        self.client.write_config(data = {
            "role": ctx.guild.default_role.id,
        })


    @commands.group("echo")
    @commands.guild_only()
    @commands.has_permissions(administrator = True)
    async def echo(self, ctx):

        if (not ctx.invoked_subcommand):
            await ctx.send("`echo` requires a subcommand.")


    @echo.command("all")
    async def echo_all(self, ctx):
        """Print out all current settings"""

        config = self.client.config
        outputString = "\n".join("**{} =** {}".format(x, config[x]) for x in config if x != "token")

        await ctx.send(outputString)


    @echo.command("queue")
    async def echo_queue(self, ctx):
        """Print out the list of queued users for potential kick"""

        pass


def setup(client):

    client.add_cog(General(client))