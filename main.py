import discord
from discord.ext import tasks, commands
import json
from datetime import datetime
import asyncio

class RainbowKickClient(commands.AutoShardedBot):
    """RainbowKick client Auto Sharded Bot wrapper"""

    def __init__(self):

        self.config = self.read_config()
        self.user_q = []
        #self.remove_command("help")

        super().__init__(
            command_prefix = self.config["prefix"]
        )

        self.load_extension("cogs.general")
    

    async def on_ready(self):
        print("Ready.")

        for guild in self.guilds:
            for member in guild.members:
                if (self.valid_kick(member)):
                    self.append_user(member)

        self.bg_task = self.loop.create_task(self.kick_loop())

    # on_member_join
    async def on_member_join(self, member: discord.Member):
        """Prepare bot modules and data"""
        
        if (self.valid_kick(member)):
            self.append_user(member)


    async def append_user(self, member: discord.Member):
        """Append a user to the user_q"""

        self.user_q.append({
            "time": datetime.utcnow().timestamp(),
            "guild_id": member.guild.id,
            "member_id": member.id
        })


    async def valid_kick(self, member: discord.Member):
        """Check if given member has a valid role for kick or not"""

        role = member.guild.get_role(self.config["role"])

        return (
            (not self.config["role"] and len(member.roles) <= 1) or
            (not role) or
            (role not in member.roles)
        )


    async def kick_loop(self):
        """Check for inactive users and kick them on an interval"""

        await self.wait_until_ready()

        while not self.is_closed():
            print("Kicking members...")

            while self.user_q:
                timestamp = self.user_q[0]["time"]
                if (datetime.utcnow().timestamp() >= timestamp + (self.config["timeout"])):
                    kick = self.user_q.pop()
                    guild = self.get_guild(kick["guild_id"])
                    role = guild.get_role(self.config["role"])
                    member = await guild.get_member(kick["member_id"])

                    # check if the user has required role
                    if (self.valid_kick(member)):
                        print("Member doesn't have the role...kicking...")
                        await guild.kick(member, reason = self.config["kick_msg"])
                        print("Kicking user {}".format(kick["member_id"]))
                    else:
                        print("Member has the role!")
                    
                else:
                    break

            await asyncio.sleep(10) # check for kicks every minute


    def read_config(self):
        """Read config.json to Python object"""
        
        try:
            with open("config.json") as json_file:
                return json.load(json_file)
        except FileNotFoundError:
            print("Configuration file not found. Please create config.json.")


    def write_config(self, data):
        """Write data to config.json file"""
        
        for key in data:
            self.config[key] = data[key]
        
        with open("config.json", "w", encoding = "utf-8") as json_file:
            json.dump(self.config, json_file, indent = 4)


# Run client
client = RainbowKickClient()
client.run(client.config["token"])